export default function diag(x, y) {
    return sqrt(square(x) + square(y));
}