/* 
-1-
Pick what, how and where from to import ... to solve errors!
files in subfolder task1: lib.js, diag.js
*/
console.log('1st task:')
console.log(square(42));
console.log(diag(3, 4));







/* 
-2- 
console.log output should be [ 'a', 'b', 'c', 'd', 'e' ]
*/

// const arr1 = ['a', 'b'];
// const arr2 = ['c'];
// const arr3 = ['d', 'e'];
console.log('2nd task:')
console.log('output some values');






/*
-3-
console log ...(one by one) all arguments from file
args.js (task3 subfolder)
*/
console.log('3rd task:')
logAllArguments(`uno`, `due`, `tre`, `...`);




/*  
-4- 
import Dog from task4
should console.log: Animal named Snoopy (beagle)
*/
console.log('4th task:')
const dogie = new Dog('Snoopy', 'beagle');
console.log(dogie.toString());
