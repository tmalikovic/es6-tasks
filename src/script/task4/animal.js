export class Animal {
    constructor(name) {
        this.name = name;
    }
    toString() {
        return `Animal name ${this.name}`;
    }
}